# Portmanteau

The CSS-in-JS library suited for many platforms. Portmanteau aims to be
performant, well-documented and framework-independent. However, the library
itself provides adapters for the different platforms and frameworks.
